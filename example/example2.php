<?php

use BitAndBlack\SyntaxHighlighter\Factories\RulesLoadingFactory;
use BitAndBlack\SyntaxHighlighter\Loading\Manager\OutputLoaderManager;
use BitAndBlack\SyntaxHighlighter\SyntaxHighlighter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

require_once '../vendor/autoload.php';

$test = new OutputLoaderManager('../src/Rules', new RulesLoadingFactory());
$ruleSet = $test->getOutputForFile('test.php');
$ruleSet2 = $test->getOutputForLanguage('php');
$ruleSet4 = $test->getOutputForLanguage('javaScripts');

$code = '<?php

class Sample
{
    /**
     * Set the page format
     *
     * @param int $width
     * @param int $height
     * @param float $unit
     * @return Spread
     */
    public function setFormat(int $width, int $height, float $unit = IDML_UNIT_POINTS): self
    {
        $width = $width / $unit;
        $height = $height / $unit;
        
        $itemTransform = ($height / 2);
        
        $this->format = [
            \'width\' => $width, 
            \'height\' => $height
        ];
        
        $this->pageAttributes[\'GeometricBounds\'] = "0 0 $height $width";
        $this->pageAttributes[\'ItemTransform\'] = "1 0 0 1 0 $itemTransform";
        
        return $this;
    }
}
';

$syntaxHighlighter = new SyntaxHighlighter(
    $code,
    $ruleSet
);

$logger = new Logger('SyntaxHighlighter Test');
$logger->pushHandler(new StreamHandler('php://stdout'));

$syntaxHighlighter->setLogger($logger);

?>

<link href="stylesheet/php-default-dark.css" rel="stylesheet">

<pre class="php default"><code>
<?php echo $syntaxHighlighter; ?>
</code></pre>