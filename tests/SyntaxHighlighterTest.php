<?php

namespace BitAndBlack\SyntaxHighlighter\Tests;

use BitAndBlack\SyntaxHighlighter\Exception\FolderNotReadableException;
use BitAndBlack\SyntaxHighlighter\Factories\RulesLoadingFactory;
use BitAndBlack\SyntaxHighlighter\Loading\Manager\OutputLoaderManager;
use BitAndBlack\SyntaxHighlighter\Output\PHP;
use BitAndBlack\SyntaxHighlighter\SyntaxHighlighter;
use PHPUnit\Framework\TestCase;

/**
 * Class SyntaxHighlighterTest
 *
 * @package BitAndBlack\SyntaxHighlighter\Tests
 */
class SyntaxHighlighterTest extends TestCase
{
    /**
     * Tests if PHP can be handeled
     *
     * @throws FolderNotReadableException
     */
    public function testCanProcessPHP(): void
    {
        $code = '<?php echo \'Hello world!\'; ?>';

        $syntaxHighlighter = new SyntaxHighlighter(
            $code,
            new PHP()
        );
        
        self::assertEquals(
            '&lt;?php echo <span class="string">\'Hello world!\'</span><span class="keyword">;</span> ?&gt;',
            $syntaxHighlighter->getSyntax()
        );
        
        $manager = new OutputLoaderManager(
            dirname(__FILE__, 2) . '/src/Rules',
            new RulesLoadingFactory()
        );
        
        $languageBasedRuleSet = $manager->getOutputForLanguage('php');

        if (null === $languageBasedRuleSet) {
            return;
        }
        
        $syntaxHighlighter = new SyntaxHighlighter(
            $code,
            $languageBasedRuleSet
        );
        
        self::assertEquals(
            '&lt;?php echo <span class="string">\'Hello world!\'</span><span class="keyword">;</span> ?&gt;',
            $syntaxHighlighter->getSyntax()
        );
    }

    /**
     * @throws FolderNotReadableException
     */
    public function testThrowsExceptionWhenFolderNotReadable(): void
    {
        $this->expectException(FolderNotReadableException::class);
        
        new OutputLoaderManager(
            'wrongPath',
            new RulesLoadingFactory()
        );
    }
}
