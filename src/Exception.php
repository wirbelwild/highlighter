<?php

namespace BitAndBlack\SyntaxHighlighter;

use Throwable;

/**
 * Class Exception
 *
 * @package BitAndBlack\SyntaxHighlighter
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     *
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
