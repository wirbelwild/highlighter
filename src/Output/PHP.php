<?php

namespace BitAndBlack\SyntaxHighlighter\Output;

/**
 * Class PHP
 *
 * @package BitAndBlack\SyntaxHighlighter\Output
 */
class PHP implements OutputInterface
{
    private array $rules = [
        'number' => [
            '/((-)*\d+)/',
        ],
        'setget' => [
            '/(([a-zA-Z]+(_[a-zA-Z]+)*?(?=\()))/',
        ],
        'variable' => [
            '/(\$[a-zA-Z0-9]+)/',
            '/((?<=-&gt;)[a-zA-Z0-9]+)(?=\[| )/',
        ],
        'static' => [
            '/([a-zA-Z0-9]+(?=::))/',
        ],
        'keyword' => [
            '/(true|false|null|new|require|include|use|require_once|include_once|foreach|try|catch|return|((?<!&lt)(?<!&gt));|,|if|else|for|public|private|empty|function|class|self)/m',
        ],
        'string' => [
            '/(\'(.+)\')/',
            '/("(.+)")/',
        ],
        'comment' => [
            '/((\<|<)!--\s*.*?\s*--(\>|>))/',
        ],
        'docblock' => [
            '/((\/\*\*(.+)*)|(\s\*(.+)*))/',
        ],
    ];

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }
}
