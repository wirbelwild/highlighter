<?php

namespace BitAndBlack\SyntaxHighlighter\Output;

/**
 * Interface OutputInterface
 *
 * @package BitAndBlack\SyntaxHighlighter\Output
 */
interface OutputInterface
{
    /**
     * This method allows to access the rules of an output object
     *
     * @return array
     */
    public function getRules(): array;
}
