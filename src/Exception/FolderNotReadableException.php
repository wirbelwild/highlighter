<?php

namespace BitAndBlack\SyntaxHighlighter\Exception;

use BitAndBlack\SyntaxHighlighter\Exception;

/**
 * Class FolderNotReadableException
 *
 * @package BitAndBlack\SyntaxHighlighter\Exception
 */
class FolderNotReadableException extends Exception
{
    /**
     * FolderNotReadableException constructor.
     *
     * @param string $folderPath
     */
    public function __construct(string $folderPath)
    {
        parent::__construct('Folder "' . $folderPath . '" can\'t be read');
    }
}
