<?php

namespace BitAndBlack\SyntaxHighlighter\Enum;

/**
 * This class is a simple enum for the JsonLoader to find the correct keys
 */
final class JsonLoaderEnum
{
    /** @var string */
    public const EXTENSIONS = 'extensions';

    /** @var string */
    public const LANGUAGES = 'languages';

    /** @var string */
    public const RULES = 'rules';
}
