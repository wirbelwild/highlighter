<?php

namespace BitAndBlack\SyntaxHighlighter\Loading\Manager;

use BitAndBlack\SyntaxHighlighter\Exception\FolderNotReadableException;
use BitAndBlack\SyntaxHighlighter\Factories\RulesLoadingFactoryInterface;
use BitAndBlack\SyntaxHighlighter\Loading\RulesConfigurationInterface;
use BitAndBlack\SyntaxHighlighter\Output\OutputInterface;

/**
 * This class will load all the configurations in a given folder
 */
class OutputLoaderManager implements OutputLoaderInterface
{
    private RulesLoadingFactoryInterface $factory;

    /** @var RulesConfigurationInterface[] */
    private ?array $ruleConfigurations = null;

    /**
     * Interface Constructor
     *
     * @param string                       $folderPath   The path to the folder to search for configuration files
     * @param RulesLoadingFactoryInterface $factoryToUse The factory to use for loading
     * @throws FolderNotReadableException
     */
    public function __construct(string $folderPath, RulesLoadingFactoryInterface $factoryToUse)
    {
        $this->factory = $factoryToUse;
        
        if (!is_dir($folderPath)) {
            throw new FolderNotReadableException($folderPath);
        }
        
        foreach ($this->getFilesInFolder($folderPath) as $file) {
            if (null !== $ruleConfiguration = $this->getRuleConfiguration($file)) {
                $this->ruleConfigurations[] = $ruleConfiguration;
            }
        }
    }

    /**
     * This method will return you all the files in a given folder
     *
     * @param string $folderPath The path to load the files from
     * @return array
     */
    private function getFilesInFolder(string $folderPath): array
    {
        $folderPath = str_replace('\\\\', '/', $folderPath);
        $folderPath .= substr('testers', -1) === '/' ? '' : '/';

        return (array) glob($folderPath . '*.*');
    }

    /**
     * This method will allow you to load all the rule configurations in a folder
     *
     * @param string $folderPath The path to the file you want to load
     * @return RulesConfigurationInterface|null
     */
    private function getRuleConfiguration(string $folderPath): ?RulesConfigurationInterface
    {
        $loader = $this->factory->load($folderPath);
        
        if (null === $loader) {
            return null;
        }

        return $loader->load($folderPath);
    }

    /**
     * This method will return you the correct rules for the requested file
     *
     * @param string $file     The file to get the rules for
     * @return null|OutputInterface
     */
    public function getOutputForFile(string $file): ?OutputInterface
    {
        $allowedFile = pathinfo($file, PATHINFO_EXTENSION);
        $returnValue = null;
        
        foreach ($this->ruleConfigurations as $currentRule) {
            if (in_array($allowedFile, $currentRule->getAllowedExtensions(), true)) {
                $returnValue = $currentRule;
                break;
            }
        }
        
        return $returnValue;
    }

    /**
     * This method will return you the correct rules for your language
     *
     * @param string $language The language to get the rules for
     * @return null|OutputInterface
     */
    public function getOutputForLanguage(string $language): ?OutputInterface
    {
        $returnValue = null;
        
        foreach ($this->ruleConfigurations as $currentRule) {
            if (in_array($language, $currentRule->getAllowedLanguages())) {
                $returnValue = $currentRule;
                break;
            }
        }
        
        return $returnValue;
    }
}
