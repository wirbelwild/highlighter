<?php

namespace BitAndBlack\SyntaxHighlighter\Loading\Manager;

use BitAndBlack\SyntaxHighlighter\Factories\RulesLoadingFactoryInterface;
use BitAndBlack\SyntaxHighlighter\Output\OutputInterface;

/**
 * This interface will allow you to load a rule set from a given folder
 */
interface OutputLoaderInterface
{
    /**
     * Interface Constructor
     *
     * @param string                       $folderPath   The path to the folder to search for configuration files
     * @param RulesLoadingFactoryInterface $factoryToUse The factory to use for loading
     */
    public function __construct(string $folderPath, RulesLoadingFactoryInterface $factoryToUse);

    /**
     * This method will return you the correct rules for the requested file
     *
     * @param string $file     The file to get the rules for
     * @return null|OutputInterface
     */
    public function getOutputForFile(string $file): ?OutputInterface;

    /**
     * This method will return you the correct rules for your language
     *
     * @param string $language The language to get the rules for
     * @return null|OutputInterface
     */
    public function getOutputForLanguage(string $language): ?OutputInterface;
}
