<?php

namespace BitAndBlack\SyntaxHighlighter\Loading;

/**
 * class representing a loaded rule configuration
 */
class RulesConfiguration implements RulesConfigurationInterface
{
    /** @var string[] */
    private array $allowedExtensions;

    /** @var string[] */
    private array $allowedLanguages;

    private array $rules;

    /**
     * RulesConfiguration constructor
     */
    public function __construct()
    {
        $this->allowedExtensions = [];
        $this->allowedLanguages = [];
        $this->rules = [];
    }

    /**
     * This method will allow you to set the allowed extensions
     *
     * @param array $allowedExtensions The allowed extensions to add
     * @return void
     */
    public function setAllowedExtensions(array $allowedExtensions): void
    {
        $this->allowedExtensions = $allowedExtensions;
    }

    /**
     * Get a list with all the allowed extensions
     *
     * @return string[]
     */
    public function getAllowedExtensions(): array
    {
        return $this->allowedExtensions;
    }

    
    /**
     * This method will allow you to set the allowed languages
     *
     * @param array $allowedLanguages The allowed languages to add
     * @return void
     */
    public function setAllowedLanguages(array $allowedLanguages): void
    {
        $this->allowedLanguages = $allowedLanguages;
    }

    /**
     * Get a list with all the allowed languages
     *
     * @return string[]
     */
    public function getAllowedLanguages(): array
    {
        return $this->allowedLanguages;
    }

    /**
     * This method will allow you to add a new rule to the current rule set
     *
     * @param string $name  The name of the new rule
     * @param string $regex The regex of the rule you want to add
     * @return void
     */
    public function addRule(string $name, string $regex): void
    {
        $this->rules[$name][] = $regex;
    }

    /**
     * This method allows to access the rules of an output object
     *
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }
}
