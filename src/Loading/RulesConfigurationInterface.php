<?php

namespace BitAndBlack\SyntaxHighlighter\Loading;

use BitAndBlack\SyntaxHighlighter\Output\OutputInterface;

/**
 * Interface representing a loaded rule configuration
 */
interface RulesConfigurationInterface extends OutputInterface
{
    /**
     * RulesConfigurationInterface
     */
    public function __construct();

    /**
     * This method will allow you to set the allowed extensions
     *
     * @param array $allowedExtensions The allowed extensions to add
     * @return void
     */
    public function setAllowedExtensions(array $allowedExtensions): void;

    /**
     * Get a list with all the allowed extensions
     *
     * @return string[]
     */
    public function getAllowedExtensions(): array;

    /**
     * This method will allow you to set the allowed languages
     *
     * @param array $allowedLanguages The allowed languages to add
     * @return void
     */
    public function setAllowedLanguages(array $allowedLanguages): void;

    /**
     * Get a list with all the allowed languages
     *
     * @return string[]
     */
    public function getAllowedLanguages(): array;

    /**
     * This method will allow you to add a new rule to the current rule set
     *
     * @param string $name  The name of the new rule
     * @param string $regex The regex of the rule you want to add
     * @return void
     */
    public function addRule(string $name, string $regex): void;
}
