<?php

namespace BitAndBlack\SyntaxHighlighter\Loading\Loaders;

use BitAndBlack\SyntaxHighlighter\Enum\JsonLoaderEnum;
use BitAndBlack\SyntaxHighlighter\Loading\RulesConfiguration;
use BitAndBlack\SyntaxHighlighter\Loading\RulesConfigurationInterface;

/**
 * This class is used to load a json formated configuration
 */
class JsonLoader implements ConfigurationLoaderInterface
{
    /** @var RulesConfigurationInterface */
    private $loadedConfiguration;

    /**
     * JsonLoader constructor
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * This method will init the instance of this loader again
     *
     * @return void
     */
    public function init(): void
    {
        $this->loadedConfiguration = null;
    }

    /**
     * This method will load a configuration file and return you the correct rule set
     *
     * @param string $filePath The path to the file to load
     * @return null|RulesConfigurationInterface
     */
    public function load(string $filePath): ?RulesConfigurationInterface
    {
        if (null !== $this->loadedConfiguration) {
            return $this->loadedConfiguration;
        }
        
        if (!file_exists($filePath)) {
            return null;
        }

        $content = file_get_contents($filePath);
        $transformed = json_decode((string) $content, true, 512, JSON_THROW_ON_ERROR);

        if (null === $transformed) {
            return null;
        }

        $this->loadedConfiguration = new RulesConfiguration();
        $this->loadedConfiguration->setAllowedExtensions($transformed[JsonLoaderEnum::EXTENSIONS]);
        $this->loadedConfiguration->setAllowedLanguages($transformed[JsonLoaderEnum::LANGUAGES]);
        
        foreach ($transformed[JsonLoaderEnum::RULES] as $name => $ruleSet) {
            foreach ($ruleSet as $regexRule) {
                $this->loadedConfiguration->addRule($name, $regexRule);
            }
        }

        return $this->loadedConfiguration;
    }
}
