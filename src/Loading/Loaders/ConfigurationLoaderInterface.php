<?php

namespace BitAndBlack\SyntaxHighlighter\Loading\Loaders;

use BitAndBlack\SyntaxHighlighter\Loading\RulesConfigurationInterface;

/**
 * This interface represents a loader for a file type
 */
interface ConfigurationLoaderInterface
{
    /**
     * ConfigurationLoaderInterface constructor
     */
    public function __construct();

    /**
     * This method will init the instance of this loader again
     *
     * @return void
     */
    public function init(): void;

    /**
     * This method will load a configuration file and return you the correct rule set
     *
     * @param string $filePath The path to the file to load
     * @return null|RulesConfigurationInterface
     */
    public function load(string $filePath): ?RulesConfigurationInterface;
}
