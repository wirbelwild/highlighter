<?php

namespace BitAndBlack\SyntaxHighlighter\Factories;

use BitAndBlack\SyntaxHighlighter\Loading\Loaders\ConfigurationLoaderInterface;

/**
 * Interface representing a loaded rule configuration
 */
interface RulesLoadingFactoryInterface
{
    /**
     * This method will allow you to get a loader for the current file type
     *
     * @param string $fileToLoad The file to get a loader for
     * @return null|ConfigurationLoaderInterface
     */
    public function load(string $fileToLoad): ?ConfigurationLoaderInterface;
}
