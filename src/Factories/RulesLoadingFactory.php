<?php

namespace BitAndBlack\SyntaxHighlighter\Factories;

use BitAndBlack\SyntaxHighlighter\Loading\Loaders\ConfigurationLoaderInterface;
use BitAndBlack\SyntaxHighlighter\Loading\Loaders\JsonLoader;

/**
 * This factory will return you the correct loader for a config file to load
 */
class RulesLoadingFactory implements RulesLoadingFactoryInterface
{
    /**
     * This method will allow you to get a loader for the current file type
     *
     * @param string $fileToLoad The file to get a loader for
     * @return null|ConfigurationLoaderInterface
     */
    public function load(string $fileToLoad): ?ConfigurationLoaderInterface
    {
        $returnClass = null;
        $fileExtension = pathinfo($fileToLoad, PATHINFO_EXTENSION);
        switch ($fileExtension) {
            case 'json':
                $returnClass = new JsonLoader();
                break;
        }

        return $returnClass;
    }
}
