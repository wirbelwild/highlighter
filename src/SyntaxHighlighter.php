<?php

namespace BitAndBlack\SyntaxHighlighter;

use BitAndBlack\SyntaxHighlighter\Output\OutputInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class SyntaxHighlighter
 *
 * @package BitAndBlack\SyntaxHighlighter
 */
class SyntaxHighlighter implements LoggerAwareInterface
{
    private array $found = [];

    private ?string $codeConverted = null;

    private string $codeOrigin;

    /**
     * Loggers instance
     */
    private LoggerInterface $logger;

    private OutputInterface $output;

    private array $nestedElements = [];

    /**
     * SyntaxHighlighter constructor.
     *
     * @param string $codeOrigin
     * @param OutputInterface $output
     */
    public function __construct(string $codeOrigin, OutputInterface $output)
    {
        $this->logger = new NullLogger();
        $this->codeOrigin = $this->specialChars($codeOrigin);
        $this->output = $output;
    }

    /**
     * Sets a logger instance
     *
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
        $this->logger->debug('Init SyntaxHighlighter');
    }

    /**
     * Prepare the code
     * @return void
     */
    private function process(): void
    {
        foreach ($this->output->getRules() as $styleName => $rules) {
            $matches = [];
            
            foreach ($rules as $ruleKey => $rule) {
                $matches[$ruleKey] = [];
                preg_match_all(
                    $rule,
                    $this->codeOrigin,
                    $matches[$ruleKey],
                    PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER
                );
            }

            $this->found[$styleName] = $matches;
        }

        $styleNames = array_keys($this->output->getRules());

        foreach ($styleNames as $styleName) {
            foreach ($this->found[$styleName] as $key => $matches) {
                foreach ($matches as $matchesKey => $match) {
                    /**
                     * Remove from array if regex had no matches
                     */
                    if (empty($match)) {
                        unset($this->found[$styleName][$key][$matchesKey]);
                        continue;
                    }

                    /**
                     * Checks the match groups and removes duplicates
                     */
                    foreach ($match as $matchKey => $found) {
                        if (0 !== $matchesKey || !is_array($found) || '' == $found[0]) {
                            unset($this->found[$styleName][$key][$matchesKey][$matchKey]);
                        }
                    }
                }

                /**
                 * Removes empty groups
                 */
                if (empty($matches)) {
                    unset($this->found[$styleName][$key]);
                    continue;
                }
            }
        }
        
        $this->found = $this->flatten($this->found);
        usort($this->found, [$this, 'sortPosition']);
    }

    /**
     * Change to a one-dimensional array
     *
     * @param array $input
     * @return array
     */
    private function flatten(array $input): array
    {
        $output = [];
        
        foreach ($input as $styleName => $rules) {
            foreach ($rules as $rule) {
                foreach ($rule as $matches) {
                    foreach ($matches as $match) {
                        $output[] = [
                            'code' => $match[0],
                            'position' => $match[1],
                            'style' => $styleName,
                        ];
                    }
                }
            }
        }
        
        return $output;
    }

    /**
     * Sort by position and length of code
     *
     * @param array $part1
     * @param array $part2
     * @return int
     */
    private function sortPosition(array $part1, array $part2): int
    {
        if ($part1['position'] === $part2['position']) {
            if (strlen($part1['code']) < strlen($part2['code'])) {
                return 1;
            }
            
            if (strlen($part1['code']) > strlen($part2['code'])) {
                return -1;
            }
            
            return 0;
        }
        
        if ($part1['position'] > $part2['position']) {
            return 1;
        }
        
        if ($part1['position'] < $part2['position']) {
            return -1;
        }
        
        return 0;
    }

    /**
     * Changes the position of each element
     *
     * @param array $found
     * @param int $position
     * @param int $before
     * @param int $after
     * @param int $keyOrigin
     * @return void
     */
    private function changePosition(array &$found, int $position, int $before, int $after, int $keyOrigin): void
    {
        $size = count($found);
        
        foreach ($found as $key => &$part) {
            $this->logger->debug("\t" . 'Handling Key ' . $key);
            
            $positionCurrent = $part['position'];
            $codeCurrent = strlen($part['code']);
            $correction = 0;
            
            if ($positionCurrent < $position) {
                $this->logger->debug("\t" . "\t" . 'Skipped ' . $key . ' because position ' . $positionCurrent . ' is smaller ' . $position);
                continue;
            }
            
            if (isset($part['correct']) && !isset($part['handeled'])) {
                $correction = $part['correct'];
                unset($part['correct']);
                $part['handeled'] = true;
                $this->logger->debug("\t" . "\t" . 'Corrected ' . $key . ': ' . $correction);
            }
    
            $positionNew = $positionCurrent + $before + $after + $correction;
            $this->logger->debug("\t" . "\t" . $positionNew . ' = ' . $positionCurrent . ' + ' . $before . ' + ' . $after . ' + ' . $correction);
    
            if ($key >= $keyOrigin) {
                $this->logger->debug("\t" . 'Looking for following nested elements inside key ' . $key);
                $this->handleNestedElements($found, $key, $positionCurrent, $codeCurrent, $size, $after, $keyOrigin);
            }
    
            $part['position'] = $positionNew;
            $found[$key] = $part;

            $this->logger->debug("\t" . "\t" . 'Position from ' . $positionCurrent . ' to ' . $positionNew);
        }
    }

    /**
     * Handles all nested elements which are inside another one
     *
     * @param array $found
     * @param int $key
     * @param int $positionCurrent
     * @param int $codeCurrent
     * @param int $size
     * @param int $after
     * @param int $keyOrigin
     * @return void
     */
    private function handleNestedElements(
        array &$found,
        int $key,
        int $positionCurrent,
        int $codeCurrent,
        int $size,
        int $after,
        int $keyOrigin
    ): void {
        for ($counter = $key + 1; $counter < $size; $counter++) {
            $next = $found[$counter];
            $positionNext = $next['position'];
            $codeNext = strlen($next['code']);
            
            $elementNested = ($positionCurrent + $codeCurrent) >= ($positionNext + $codeNext);
    
            $this->logger->debug(
                "\t" . "\t" . 'Key ' . $counter . ' is nested: ' .
                var_export($elementNested, true) . '. ' .
                ($positionCurrent + $codeCurrent) . ' >= ' . ($positionNext + $codeNext)
            );
            
            if ($elementNested && $keyOrigin == $key) {
                $nestedValue = $this->nestedElements[$counter] ?? 0;
                $this->nestedElements[$counter] = $nestedValue + 1;
            }
            
            /**
             * If nesting level is >1 this will proof that the negative position is set correctly
             */
            if ($elementNested && isset($found[$counter]['handeled']) && true === $found[$counter]['handeled']) {
                /**
                 * Run negative position once again when parent element is reached
                 */
                if ($keyOrigin == $key && $this->nestedElements[$counter] >= 1) {
                    unset($found[$counter]['handeled']);
                }
            }

            /**
             * Aborts when nesting ends
             */
            if (!$elementNested) {
                $this->logger->debug("\t" . "\t" . 'No nested element found after key ' . $counter . ', abort.');
                break;
            }

            /**
             * Sets the negative correction of nested elements only from their parent elements
             */
            if ($keyOrigin == $key) {
                $found[$counter]['correct'] = -$after;
            }
        }
    }

    /**
     * Loop through the array of matches
     * @return void
     */
    private function createSyntax(): void
    {
        $this->codeConverted = $this->codeOrigin;
        $this->process();
        
        foreach ($this->found as $key => &$part) {
            $code = $part['code'];
            $position = $part['position'];
            $style = $part['style'];
            
            $codeBefore = '<span class="' . $style . '">';
            $codeAfter = '</span>';
            $codeFull = $codeBefore . $code . $codeAfter;
            
            $this->codeConverted = substr_replace($this->codeConverted, $codeFull, $position, strlen($code));
        
            $this->logger->debug($key . ' ============= ');
            
            $this->changePosition($this->found, $position, strlen($codeBefore), strlen($codeAfter), $key);
            $this->logger->debug("\t" . "\t" . "\t" . "\t" . $this->codeConverted);
        }
    }

    /**
     * Returns the syntaxed code
     * @return string
     */
    public function getSyntax(): string
    {
        if (null === $this->codeConverted) {
            $this->createSyntax();
        }
        
        return (string) $this->codeConverted;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getSyntax();
    }

    /**
     * @param string $codeOrigin
     * @return string
     */
    private function specialChars(string $codeOrigin): string
    {
        return str_replace(['<', '>'], ['&lt;', '&gt;'], $codeOrigin);
    }
}
