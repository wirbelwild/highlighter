[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/syntaxhighlighter)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/f702ffb1740b43d18a5bfe19fef88154)](https://www.codacy.com/bb/wirbelwild/highlighter/dashboard)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/syntaxhighlighter/v/stable)](https://packagist.org/packages/bitandblack/syntaxhighlighter)
[![Total Downloads](https://poser.pugx.org/bitandblack/syntaxhighlighter/downloads)](https://packagist.org/packages/bitandblack/syntaxhighlighter)
[![License](https://poser.pugx.org/bitandblack/syntaxhighlighter/license)](https://packagist.org/packages/bitandblack/syntaxhighlighter)

# SyntaxHighlighter

A Syntax Highlighter written in PHP using RegEx. Bringing beautiful styles. Easy to extend.

![Preview](https://bytebucket.org/wirbelwild/highlighter/raw/a5c42562347bc49fb25ea3469bc56e3b87b8a5b4/example/highlighter-preview.jpg)

## Installation

This package is available for the use with [Composer](https://packagist.org/packages/bitandblack/syntaxhighlighter). Use composer to install by running `$ composer require bitandblack/syntaxhighlighter` and `$ composer update`. 

## Usage

Set up the highlighter like that:

````php
<?php

use BitAndBlack\SyntaxHighlighter\Output\PHP;
use BitAndBlack\SyntaxHighlighter\SyntaxHighlighter;

$code = '<?php echo \'Hello world!\'; ?>';

$syntaxHighlighter = new SyntaxHighlighter(
    $code,
    new PHP()
);
````

Print the syntaxed code then:

````html
<pre><code class="default php">
<?php echo $syntaxHighlighter; ?>
</code></pre>
````

You can print the code also by using `$syntaxHighlighter->getSyntax()`. 

Note that there's no need to convert the input with `htmlspecialchars()` or something else. Highlighter will do this by its own.

### Using the dynamic language loader

If you want to use the dynamic language rule loader you need to add the following code before calling the `SyntaxHighlighter` class.

```PHP
<?php

use BitAndBlack\SyntaxHighlighter\Factories\RulesLoadingFactory;
use BitAndBlack\SyntaxHighlighter\Loading\Manager\OutputLoaderManager;
use BitAndBlack\SyntaxHighlighter\SyntaxHighlighter;

require_once 'vendor/autoload.php';

// The first argument is the folder where the rules are saved
// The second argument is the factory to get the loader classes for the files in the rules folder
$manager = new OutputLoaderManager('src/Rules', new RulesLoadingFactory());

// Both methods will return null if there was no rule set found!
$fileBasedRuleSet = $manager->getOutputForFile('test.php');
$languageBasedRuleSet = $manager->getOutputForLanguage('php');

$syntaxHighlighter = new SyntaxHighlighter(
    $code,
    //You can use the $fileBasedRuleSet rule set as well here!
    $languageBasedRuleSet
);

```

There is only a rule for php at the moment, if you want to create rules on your own use the following json template to do so

```JSON
{
    "extensions": [
        "php"
    ],
    "languages": [
        "php"
    ],
    "rules": {
        "number": [
            "/((-)*\\d+)/"
        ],
        "constant": [
            "/([A-Z]{1,}[A-Z0-9_]{1,}\\w*(\\()?(\\))?)/"
        ],
        "setget": [
            "/(([a-zA-Z]+(_[a-zA-Z]+)*?(?=\\()))/"
        ],
        "variable": [
            "/(\\$[a-zA-Z0-9]+)/",
            "/((?<=-&gt;)[a-zA-Z0-9]+)(?=\\[| )/"
        ],
        "static": [
            "/([a-zA-Z0-9]+(?=::))/"
        ],
        "keyword": [
            "/(true|false|null|new|require|include|use|require_once|include_once|foreach|try|catch|return|((?<!&lt)(?<!&gt));|,|if|else|for|public|private|empty|function|class|self)/m"
        ],
        "string": [
            "/(\\'(.+)\\')/",
            "/(\"(.+)\")/"
        ],
        "comment": [
            "/((\\<|<)!--\\s*.*?\\s*--(\\>|>))/"
        ],
        "docblock": [
            "/((\\/\\*\\*(.+)*)|(\\s\\*(.+)*))/"
        ],
        "default": [
            "/((?=use )([^;]*))/"
        ]
    }
}
```

*   Values listed in the `extensions` key will list all the allowed files extensions for the rule set
*   Values listed in the `languages` key will define all the languages allowed to use with this rule set
*   Values listed below the `rules` key will define the rules the highlighter will use for highlighting

### Styling

There's a different package holding some nice styles written in `scss` which can be used with NPM or Yarn. This makes it easier to add them to your project.

You can find the packe under [www.npmjs.com/package/bitandblack-highlighter](https://www.npmjs.com/package/bitandblack-highlighter).

## Writing a custom output

It's possible to write a custom output with custom regex rules. This class needs to implement the `OutputInterface`.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).